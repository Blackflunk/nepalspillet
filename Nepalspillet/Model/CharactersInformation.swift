//
//  FigureData.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 26/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import UIKit

struct CharactersInformation: Codable{
    var figurer: [Karakter]
    var characterQuestions: [CharacterQuestion]
}
    
    struct Karakter: Codable{
        var navn: String
        var drengekøn: String
        var fuldtNavn: String
        var beskrivelse: String
        var beskrivelse_hjemme: String
        var startpenge: Int
        var startviden: Int
        var uheld: [Uheld]
}
    
    
    struct Uheld: Codable {
        var titel: String?
        var tekst: String?
        var tidFaktor: Double?
        var pengeForskel: Double?
        var videnForskel: Double?
        var madForskel: Double?
    }

    struct CharacterQuestion: Codable {
        var question: String
        var answerFirst: String
        var answerSecond: String
        var answerThird: String
        var trueAnswer: String
    }
