//
//  CharacterSelectorFigure.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 18/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class CharacterSelectorFigure {
    let characterFigureNameNode, characterDescriptionNameNode, characterBubbleAreaNode : SKNode
    let figurePictureHalf, figurePictureFull : UIImage
    
    init(name : SKNode, description : SKNode, bubble : SKNode, figurePictureHalf : UIImage, figurePictureFull : UIImage) {
        self.characterFigureNameNode = name
        self.characterDescriptionNameNode = description
        self.characterBubbleAreaNode = bubble
        self.figurePictureHalf = figurePictureHalf
        self.figurePictureFull = figurePictureFull
    }
}
