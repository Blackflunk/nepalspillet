//
//  Character.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 12/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class PlayerCharacter {
    var name : String
    var educationPencil, educationNotebook, educationCalculator, forgottenKnowledge, books, position, money, food, educationalKnowledge, educationalLevel, time, turn, booksBoughtInRoundNumber : Int
    var characterInformation: Karakter
    var hasBike: Bool
    static let BOARDSIZE = 8

    init(name: String, money: Int, characterInformation: Karakter) {
        educationPencil = 0
        educationNotebook = 0
        educationCalculator = 0
        forgottenKnowledge = 0
        books = 0
        position = 0
        food = 100
        educationalKnowledge = 0
        educationalLevel = 1
        time = 19
        turn = 1
        booksBoughtInRoundNumber = 0
        hasBike = false
        
        self.name = name
        self.money = money
        self.characterInformation = characterInformation
    }
    
    func work(earnedMoney: Int, timeTaken: Int) {
        money += earnedMoney
        time -= timeTaken
    }
    
    func study(earnedKnowledge: Int, timeTaken: Int, ungainedKnowledge: Int){
        educationalKnowledge += earnedKnowledge
        time -= timeTaken
        forgottenKnowledge += ungainedKnowledge
    }
    
    func eat(foodEaten: Int, foodPrice: Int, timeTaken: Int){
        food += foodEaten
        money -= foodPrice
        time -= timeTaken
    }
    
    func walk(){
        time -= 1
    }
    
    func bike(indexInPath: Int){
        // Having a bike makes you only lose time for every second field
        if(hasBike && ModulusHelper.sharedInstance.mod(indexInPath, 2) == 1){
            // Nothing happens every second turn if a bike is present
            return
        } else {
            walk()
        }
    }
    
    func increaseWeek(isHungry: Bool){
        if(isHungry){
            food = 0
            time = 10
        } else {
            time = 19
            food = food-30
        }
        turn += 1
    }
}
