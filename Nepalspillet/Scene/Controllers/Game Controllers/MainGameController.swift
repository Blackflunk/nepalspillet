//
//  MainGameClass.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 08/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import UIKit
import SpriteKit
import GSMessages

class MainGameController: SKScene{
    var playerCharacter: PlayerCharacter!
    var characterFigure: SKSpriteNode?
    var passedFigureData: CharacterSelectorFigure!
    var fieldNodes: [SKNode]?
    var currentField: SKNode!
    var movingToField: Bool = false
    var gameHUD: SKNode?
    var isInitialised: Bool = false
    
    // According to spritekit documentation, it's not really possible to just initialise the scene.
    override func didMove(to view: SKView) {
        // Have to make check to insure that the view is not initialised several times to keep the same game state.
        if(!isInitialised){
            // Take receive passed data
            passedFigureData = self.userData?.value(forKey: "figureInformation") as? CharacterSelectorFigure
            let passedCharacterInformation = self.userData?.value(forKey: "characterInformation") as? Karakter
            fieldNodes = childNode(withName: "fields")?.children
            
            // Create visual character
            let characterTextureHalf = SKTexture(image: (passedFigureData?.figurePictureHalf)!)
            characterFigure = SKSpriteNode(texture: characterTextureHalf, size: CGSize(width: 100, height: 150))
            
            // If character exists, set the z position to 4 as it has to be on top of the board.
            if let character = characterFigure {
                characterFigure?.zPosition = 4
                addChild(character)
                
                // Move character to start pisiton
                if let fields = fieldNodes{
                    let spriteNodeFields = fields as! [SKSpriteNode]
                    let homeNode = spriteNodeFields[0]
                    character.position = homeNode.position
                    currentField = homeNode
                } else {
                    print("Cannot find fields")
                }
            }
            
            //Define character information from the character information receied.
            if let characterInformation = passedCharacterInformation{
                playerCharacter = PlayerCharacter(name: characterInformation.navn, money: characterInformation.startpenge, characterInformation: characterInformation)
            } else {
                print("Cannot create playercharacter")
            }
            
            // Instantiates the HUD: The returned array contains food, knowledge, money and remaining turns
            gameHUD = UIHelper.sharedInstance.createTopBarUI(scene: self)
            UIHelper.sharedInstance.updateHUD(labels: gameHUD!.children, playerCharacter: playerCharacter)
            isInitialised = true
        } else {
            playerCharacter = self.userData?.value(forKey: "returnedPlayerCharacter") as? PlayerCharacter
            UIHelper.sharedInstance.updateHUD(labels: gameHUD!.children, playerCharacter: playerCharacter)
        }
        
        // We want to update the timer everytime the view appears
        updateVisualTimer()
        
        if(playerCharacter.educationalLevel == 10){
            let wonGameAlert: UIAlertController = UIAlertController(title: "Du har vundet spillet!", message: "Tillykke du har vundet spillet ved at komme til 10 klasse! Du færdiggjorde spillet på " + String(playerCharacter.turn) + " ture.", preferredStyle: .alert)
            
            wonGameAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { alert in self.leaveGame()}))
            self.view?.window?.rootViewController?.present(wonGameAlert, animated: true, completion: nil)
        }
    }
    
    // When a touch is registered
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            if(atPoint(location) == currentField && !movingToField){
                enterLocation()
            }
            
            // Back button
            if(atPoint(location) == gameHUD?.children[4]){
                let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Forlad spillet?", message:"", preferredStyle: .alert)
                noMoreTimeAlert.addAction(UIAlertAction(title: "Ja", style: .destructive, handler: {action in self.leaveGame()}))
                noMoreTimeAlert.addAction(UIAlertAction(title: "Nej", style: .cancel, handler: {
                    action in noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                
                self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
            }
            else if let fields = fieldNodes{
                for field in fields{
                    // Check if a field is pressed in the array of fields
                    if atPoint(location) == field && !movingToField{
                        movingToField = true
                        let path = QuickestPathFinder.sharedInstance.findPath(fields: fields, startPos: currentField, endPos: field)
                        walkPath(path: path!, nextNode: 0)
                        
                        
                    }
                }
            }
        }
    }
    
    func leaveGame(){
        if let scene = MainMenuController(fileNamed: "MainMenuScene"){
            // Scale the window to fit.
            scene.scaleMode = .aspectFill
            
            // Present the scene
            view!.presentScene(scene, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
        }
    }
    
    // Moves the character given a path and an index of the next node in the path
    func walkPath(path: [SKNode], nextNode: Int){
        // Walk until we are there (If nextNode is greater than, then we're there)
        if(nextNode<path.count){
            // If there's time, we walk.
            if(playerCharacter.time > 0){
                // Since we can walk to the next node, we can say the currentfield is the next node.
                currentField = path[nextNode]
                let action = SKAction.move(to: path[nextNode].position, duration: 0.5)
                action.timingMode = .easeInEaseOut
                
                if(!playerCharacter.hasBike){
                    playerCharacter.walk()
                } else {
                    playerCharacter.bike(indexInPath: nextNode)
                }
                
                UIHelper.sharedInstance.updateHUD(labels: gameHUD!.children, playerCharacter: playerCharacter)
                updateVisualTimer()
                
                // This will run an action that moves the character on the game board, it runs recursively using the completion block.
                characterFigure?.run(action, completion: {
                    self.walkPath(path: path, nextNode: nextNode+1)
                })
            } else {
                endOfWeekEvent()
                movingToField = false
            }
        } else {
            // When figure is at destination, do the following:
            movingToField = false
            enterLocation()
            
        }
    }
    
    func enterLocation() {
        var scene: SKScene?
        
        // Determine which scene to enter
        if let fieldToEnter = currentField{
            switch fieldToEnter.name{
            case Constants.homeField.rawValue?:
                scene = HomeController(fileNamed: "HomeScene")
            case Constants.shopField.rawValue?:
                scene = ShopController(fileNamed: "ShopScene")
            case Constants.groceriesField.rawValue?:
                scene = GroceryController(fileNamed: "GroceryScene")
            case Constants.riceField.rawValue?:
                scene = RiceController(fileNamed: "RiceScene")
            case Constants.schoolField.rawValue?:
                scene = SchoolController(fileNamed: "SchoolScene")
            case Constants.bookstoreField.rawValue?:
                scene = BookshopController(fileNamed: "BookshopScene")
            case Constants.bicycleWorkshop.rawValue?:
                scene = BicycleworkshopController(fileNamed: "BicycleworkshopScene")
            case Constants.privateTutoring.rawValue?:
                scene = PrivatetutoringController(fileNamed: "PrivatetutoringScene")
            default: break
            }
        }
        // Present scene if it found one
        if let sceneToPresent = scene{
            // Add data about the character, turns and also the mainGameController is sent in order to retrieve the same game state when going back.
            sceneToPresent.userData = ["characterInformation": playerCharacter!, "remainingTurns": playerCharacter!.time, "mainGameController": self, "passedFigureData": passedFigureData]
            
            // Scale the window to fit.
            sceneToPresent.scaleMode = .aspectFill
            
            // Present the scene
            view!.presentScene(sceneToPresent, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
        } else {
            print("Could not find scene to present")
        }
    }
    
    func updateVisualTimer() {
        let gameTimerNode = self.childNode(withName: "gameTimer") as! SKSpriteNode
        let gameTimerLabelDayNode = self.childNode(withName: "timerLabelDay") as! SKLabelNode
        
        // Update the week number
        let gameTimerLabelWeekNode = self.childNode(withName: "timerLabelWeek") as! SKLabelNode
        gameTimerLabelWeekNode.text = "Uge: " + String(playerCharacter.turn)
        
        for i in 0...19{
            if(playerCharacter?.time == i){
                gameTimerNode.texture = SKTexture(imageNamed: "ur"+String(i))
                gameTimerLabelDayNode.text = String(i)
            }
        }
    }
    
    func endOfWeekEvent(){
        //Check for character events
        if(playerCharacter.food - 30 <= 0){
            // Show alert for week over:
            view?.showMessage(playerCharacter.name + " " + "har ikke spist nok denne uge og har derfor mindre ture denne gang.", type: .error, options: [.position(.bottom)])
            
            
            // Update time for next week:
            playerCharacter.increaseWeek(isHungry: true)
            
        } else if(playerCharacter.food - 30 > 0) {
            view?.showMessage("Ugen er ovre!", type: .info, options: [.position(.bottom)])
            
            //Update character for next week
            playerCharacter.increaseWeek(isHungry: false)
        }
        // A random event triggers every fifth week
        if(playerCharacter.turn % 5 == 0){
            randomEvent()
        }
        
        // Move figure
        currentField = fieldNodes?[0]
        let action = SKAction.move(to: fieldNodes![0].position, duration: 0.5)
        action.timingMode = .easeInEaseOut
        characterFigure?.run(action)
        
        //Update UI
        updateVisualTimer()
        UIHelper.sharedInstance.updateHUD(labels: gameHUD!.children, playerCharacter: playerCharacter)
        
    }
    
    func randomEvent(){
        // Following is needed in order to create a random number given the count of the events
        let randomEventIndex = Int(arc4random_uniform(UInt32(playerCharacter.characterInformation.uheld.count)))
        
        print(playerCharacter.characterInformation.uheld.count)
        
        let accidentEvent = playerCharacter.characterInformation.uheld[randomEventIndex]
        
        print(accidentEvent)
        
        // The following check is made in order to insure that an accident can happen.
        if let moneyEventMoney = accidentEvent.pengeForskel{
            if (Int(moneyEventMoney)+playerCharacter.money < 0){
                return
            }else{
                playerCharacter.money = Int(moneyEventMoney) + playerCharacter.money
            }
        }
        if let knowledgeEventKnowledge = accidentEvent.videnForskel{
            if (Int(knowledgeEventKnowledge) + playerCharacter.educationalKnowledge < 0){
                return
            }else{
                playerCharacter.educationalKnowledge = Int(knowledgeEventKnowledge) + playerCharacter.educationalKnowledge
            }
        }
        if let foodEventFood = accidentEvent.madForskel{
            if(Int(foodEventFood) + playerCharacter.food < 0){
                return
            }else{
                playerCharacter.food = Int(foodEventFood) + playerCharacter.food
            }
        }
        if let timeEventTime = accidentEvent.tidFaktor{
            if(timeEventTime < 0.5){
                playerCharacter.time = 0
            } else{
                let newTime = timeEventTime*Double(playerCharacter.time)
                playerCharacter.time = Int(newTime)
            }
        }
        
        let accidentAlert: UIAlertController = UIAlertController(title: accidentEvent.titel, message:accidentEvent.tekst, preferredStyle: .alert)
        
        accidentAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
            alert in accidentAlert.dismiss(animated: true, completion: nil)}))
        self.view?.window?.rootViewController?.present(accidentAlert, animated: true, completion: nil)
        
    }
    
    
    // Used to determine which field to enter
    enum Constants: String {
        case homeField = "field_home"
        case shopField = "field_shop"
        case groceriesField = "field_groceries"
        case riceField = "field_rice"
        case schoolField = "field_school"
        case bookstoreField = "field_bookstore"
        case bicycleWorkshop = "field_bicycleworkshop"
        case privateTutoring = "field_privatetutoring"
    }
    
}
