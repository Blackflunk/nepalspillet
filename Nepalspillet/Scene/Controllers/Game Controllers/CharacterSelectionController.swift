//
//  CharacterSelectionScene.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 12/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import UIKit
import SpriteKit

class CharacterSelectionController: SKScene {
    
    var characterNodeElements = [CharacterSelectorFigure]()
    var descriptionBarNode: SKNode?
    var previousChosenNodeName: String = ""
    var characterIsSelected: Bool?
    var playButton: SKNode?
    var playButtonLabel: SKLabelNode?
    let fadeDuration = 0.2
    
    override func didMove(to view: SKView) {
        
        populateCharacterNodes()
        characterIsSelected = false
        
        if let playGameButton = self.childNode(withName: "PlayGameButton"){
            playButton = playGameButton
        }
        
        // The child node of the label has to be type casted to type SKLabelNode instead of SKNode, also we assign the label variable.
        if let buttonLabel = playButton?.childNode(withName: "PlayGameButtonText") as? SKLabelNode {
            playButtonLabel = buttonLabel
        }
    }
    
    // Characters are hardcoded TODO: Populate using the grunddata.json?
    func populateCharacterNodes (){
        characterNodeElements = [
            CharacterSelectorFigure(name: childNode(withName: "Kamal")!,description: childNode(withName: "KamalDescription")!,bubble: childNode(withName: "KamalBubble")!, figurePictureHalf: #imageLiteral(resourceName: "figur_kamal_halv"), figurePictureFull: #imageLiteral(resourceName: "figur_kamal_hel")),
            CharacterSelectorFigure(name: childNode(withName: "Laxmi")!,description: childNode(withName: "LaxmiDescription")!,bubble: childNode(withName: "LaxmiBubble")!, figurePictureHalf: #imageLiteral(resourceName: "figur_laxmi_halv"), figurePictureFull: #imageLiteral(resourceName: "figur_laxmi_hel")),
            CharacterSelectorFigure(name: childNode(withName: "Krishna")!,description: childNode(withName: "KrishnaDescription")!,bubble: childNode(withName: "KrishnaBubble")!, figurePictureHalf: #imageLiteral(resourceName: "figur_krishna_halv"), figurePictureFull: #imageLiteral(resourceName: "figur_krishna_hel")),
            CharacterSelectorFigure(name: childNode(withName: "Asha")!,description: childNode(withName: "AshaDescription")!,bubble: childNode(withName: "AshaBubble")!, figurePictureHalf: #imageLiteral(resourceName: "figur_asha_halv"), figurePictureFull: #imageLiteral(resourceName: "figur_asha_hel"))]
        
        descriptionBarNode = childNode(withName: "DescriptionBar")
    }
    
    func focusCharacter (focusedCharacterNode: SKNode){
        // Ensure a character is not already selected
        if(!characterIsSelected!){
            for node in characterNodeElements {
                
                // If it is the selected character
                if(node.characterFigureNameNode.name == focusedCharacterNode.name){
                    node.characterBubbleAreaNode.run(SKAction.fadeAlpha(to: 1, duration: fadeDuration))
                    
                }
                // If it is the other characters
                if(node.characterFigureNameNode.name != focusedCharacterNode.name){
                    node.characterFigureNameNode.run(SKAction.fadeAlpha(to: 0.5, duration: fadeDuration))
                    node.characterDescriptionNameNode.run(SKAction.fadeAlpha(to: 0.5, duration: fadeDuration))
                }
                characterIsSelected = true
                // Set chosen node name
                previousChosenNodeName = focusedCharacterNode.name!
                
                // Fade in the play button
                playButton?.run(SKAction.fadeIn(withDuration: fadeDuration))
                playButtonLabel?.text = NSString(format: "Spil som %@.", previousChosenNodeName) as String
            }
        }
    }
    
    func unfocusCharacter (){
        if(characterIsSelected!){
            for node in characterNodeElements{
                // For all other character figures
                if(node.characterFigureNameNode.name != previousChosenNodeName){
                    node.characterFigureNameNode.run(SKAction.fadeAlpha(to: 1, duration: fadeDuration))
                    node.characterDescriptionNameNode.run(SKAction.fadeAlpha(to: 1, duration: fadeDuration))
                } else {
                    // For the selected character
                    node.characterBubbleAreaNode.run(SKAction.fadeAlpha(to: 0, duration: fadeDuration))
                }
            }
        }
        characterIsSelected = false
        // Reset the previous chosen node so that the same person can be chosen again.
        previousChosenNodeName = ""
        
        // Fade out the play button
        playButton?.run(SKAction.fadeOut(withDuration: fadeDuration))
        
    }
    
    func findFigureUsing(name: String) -> CharacterSelectorFigure?{
        for characterFigure in characterNodeElements{
            if(characterFigure.characterFigureNameNode.name == name){
                return characterFigure
            }
        }
        return nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // If play button is pressed, we start game scene containing the chosen player.
            if(atPoint(location).name == playButton?.name || atPoint(location).name == playButtonLabel?.name){
                if let characterFigure = findFigureUsing(name: previousChosenNodeName){
                    // Press button animation
                    atPoint(location).run(SKAction.scale(by: 0.7, duration: 0.2))
                    
                    if let scene = MainGameController(fileNamed: "MainGameScene"){
                        // Scale the window to fit.
                        scene.scaleMode = .aspectFill
                        
                        // Retrieve and send all the informations about the character, include context from the controller to the next scene
                        if let jsonData = JsonLoader.sharedInstance.extractGameDataFrom(characterName: previousChosenNodeName) {
                            scene.userData = ["figureInformation": characterFigure, "characterInformation": jsonData]
                        }
                    
                        // Present the scene
                        view!.presentScene(scene, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
                    }
                } else {
                    print("Error: Character figure cannot be found.")
                }
            }
            
            // Run through the character nodes and check if pressed on one of them
            for node in characterNodeElements{
                // Focus character
                if(atPoint(location).name == node.characterFigureNameNode.name && atPoint(location).name != previousChosenNodeName){
                    focusCharacter(focusedCharacterNode: atPoint(location))
                    
                }
            }
            
            // If pressing outside the characters
            // Unfocus character
            if(atPoint(location).name != previousChosenNodeName){
                unfocusCharacter()
            }
        }
    }
}
