//
//  CreditsController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 26/01/2018.
//  Copyright © 2018 Alexander V. Pedersen. All rights reserved.
//

import SpriteKit

class CreditsController: SKScene {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let scene = MainMenuController(fileNamed: "MainMenuScene"){
            // Scale the window to fit.
            scene.scaleMode = .aspectFill
            
            // Present the scene
            view!.presentScene(scene, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
        }
    }
}

