//
//  MainMenu.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 08/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import SpriteKit
import UIKit

class MainMenuController: SKScene {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            if atPoint(location).name == "StartGame"{
                // Press button animation
                atPoint(location).run(SKAction.scale(by: 0.7, duration: 0.2))
                
                if let scene = CharacterSelectionController(fileNamed: "CharacterSelectionScene"){
                    // Scale the window to fit.
                    scene.scaleMode = .aspectFill
                    
                    // Present the scene
                    view!.presentScene(scene, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
                }
            }
            if atPoint(location).name == "SettingsButton"{
                let settingsAlert: UIAlertController = UIAlertController(title: "Indstillinger", message: "", preferredStyle: .alert)
                settingsAlert.addAction(UIAlertAction(title: "Akkreditering", style: UIAlertActionStyle.default, handler: {
                    action in self.goToCreditsScene()}))
                settingsAlert.addAction(UIAlertAction(title: "Besøg Skoleliv i Nepal", style: UIAlertActionStyle.default, handler: {
                    action in self.goToSkoleLiv()}))
                settingsAlert.addAction(UIAlertAction(title: "Annuller", style: UIAlertActionStyle.cancel, handler: nil))
                
                self.view?.window?.rootViewController?.present(settingsAlert, animated: true, completion: nil)
            }
        }
    }
    
    func goToCreditsScene(){
        if let scene = CreditsController(fileNamed: "CreditsScene"){
            // Scale the window to fit.
            scene.scaleMode = .aspectFill
            
            // Present the scene
            view!.presentScene(scene, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
        }
    }
    
    func goToSkoleLiv(){
        if let url = NSURL(string: "https://skoleliv-i-nepal.dk") {
            UIApplication.shared.open(url as URL, options: [:])
        }
    }
}
