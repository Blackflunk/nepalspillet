//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit
import GSMessages

class SchoolController: SKScene{
    
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode!
    
    // To study
    let KNOWLEDGE_PER_SESSION_STUDY = 1
    let TIME_PER_SESSION_STUDY = 1
    var UNGAINED_KNOWLEDGE_PER_SESSION_STUDY = 1
    
    // To eat
    var FOOD_PER_SESSION = 10
    var TIME_PER_SESSION_FOOD = 1
    var PRICE_PER_SESSION_FOOD = 0
    
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        let characterFigureData = self.userData?.value(forKey: "passedFigureData") as? CharacterSelectorFigure
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
        
        updateBlackBoard()
        
        // Character in scene
        let characterFigureNode = childNode(withName: "characterSprite") as! SKSpriteNode
        characterFigureNode.texture = SKTexture(image: (characterFigureData?.figurePictureFull)!)
        characterFigureNode.size = CGSize(width: 200, height: 500)
        characterFigureNode.zPosition = 2
    }
    
    // Parameters determine how likely the student is to understand the lesson
    func lessonUnderstoodDeterminer (knowledgePossibility: Double, tutoringPossibility: Double) {
        let randNumb = (Double(arc4random())/Double(UINT32_MAX))
        print(randNumb)
        // Either the player understands the lesson, needs tutoring or didn't understand.
        if(randNumb < knowledgePossibility){
            playerCharacter.study(earnedKnowledge: KNOWLEDGE_PER_SESSION_STUDY, timeTaken: TIME_PER_SESSION_STUDY, ungainedKnowledge: 0)
            
            // Message about gained knowledge
            view?.showMessage("Har fået " + String(KNOWLEDGE_PER_SESSION_STUDY) + " viden", type: .success, options: [
                .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                .cornerRadius(10),
                .autoHideDelay(1)])
        } else if (randNumb < tutoringPossibility){
            playerCharacter.study(earnedKnowledge: 0, timeTaken: TIME_PER_SESSION_STUDY, ungainedKnowledge: UNGAINED_KNOWLEDGE_PER_SESSION_STUDY)
            
            // Message about need for tutoring
            view?.showMessage("Mangler lektiehjælp for " + String(UNGAINED_KNOWLEDGE_PER_SESSION_STUDY) + " time.", type: .info, options: [
                .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                .cornerRadius(10),
                .autoHideDelay(1)])
        } else {
            playerCharacter.study(earnedKnowledge: 0, timeTaken: TIME_PER_SESSION_STUDY, ungainedKnowledge: 0)
            
            // Message about not understanding the lesson at all
            view?.showMessage(playerCharacter.name + " forstod ikke timen.", type: .error, options: [
                .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                .cornerRadius(10),
                .autoHideDelay(1)])
        }
    }
    // Determines which utility to use, will use the most expensive first.
    func lessonToolUtiliser() {
        if(playerCharacter.educationCalculator > 0){
            lessonUnderstoodDeterminer(knowledgePossibility: 0.7, tutoringPossibility: 1)
            playerCharacter.educationCalculator -= 1
            
            // No more calculator charges after usage?
            if(playerCharacter.educationCalculator == 0){
                // Message about not understanding the lesson at all
                let outOfChargesAlert: UIAlertController = UIAlertController(title: "Lommeregneren gik i stykker!", message: "Lommeregneren gik i stykker, gå til butikken for at købe en ny.", preferredStyle: .alert)
                outOfChargesAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                    (alertAction: UIAlertAction!) in
                    outOfChargesAlert.dismiss(animated: true, completion: nil)}))
                self.view?.window?.rootViewController?.present(outOfChargesAlert, animated: true, completion: nil)
            }
        } else if (playerCharacter.educationNotebook > 0) {
            lessonUnderstoodDeterminer(knowledgePossibility: 0.6, tutoringPossibility: 0.85)
            playerCharacter.educationNotebook -= 1
            
            // No more notebook charges after usage?
            if(playerCharacter.educationNotebook == 0){
                let outOfChargesAlert: UIAlertController = UIAlertController(title: "Kladdehæftet er tomt!", message: "Kladdehæftet har ikke flere sider, gå til butikken for at købe en ny.", preferredStyle: .alert)
                outOfChargesAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                    (alertAction: UIAlertAction!) in
                    outOfChargesAlert.dismiss(animated: true, completion: nil)}))
                self.view?.window?.rootViewController?.present(outOfChargesAlert, animated: true, completion: nil)
            }
        } else if (playerCharacter.educationPencil > 0) {
            lessonUnderstoodDeterminer(knowledgePossibility: 0.55, tutoringPossibility: 0.8)
            playerCharacter.educationPencil -= 1
            
            // No more pencil charges after usage?
            if(playerCharacter.educationPencil == 0){
                let outOfChargesAlert: UIAlertController = UIAlertController(title: "Blyanten virker ikke længere!", message: "Blyanten kan ikke skrive længere, gå til butikken for at købe en ny.", preferredStyle: .alert)
                outOfChargesAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                    (alertAction: UIAlertAction!) in
                    outOfChargesAlert.dismiss(animated: true, completion: nil)}))
                self.view?.window?.rootViewController?.present(outOfChargesAlert, animated: true, completion: nil)
            }
        } else {
            lessonUnderstoodDeterminer(knowledgePossibility: 0.5, tutoringPossibility: 0.75)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                leaveScene()
            }
            if atPoint(location) == childNode(withName: "buttonStudy"){
                // As long as the player has turns left, the player is able to work.
                if(playerCharacter.time > 0){
                    lessonToolUtiliser()
                    UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                } else {
                    // If the character has no more time, an alert will show.
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Turen er ovre.", message: "Kan ikke studere mere idag.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                    
                }
            }
            if atPoint(location) == childNode(withName: "buttonEat"){
                // As long as the player has turns left, the player is able to work.
                if(playerCharacter.time > 0){
                    playerCharacter.eat(foodEaten: FOOD_PER_SESSION, foodPrice: PRICE_PER_SESSION_FOOD, timeTaken: TIME_PER_SESSION_FOOD)
                    UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                    view?.showMessage("Har spist: " + String(FOOD_PER_SESSION) + " mad.", type: .info, options: [
                        .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                        .cornerRadius(10),
                        .autoHideDelay(0.5)])
                } else {
                    // If the character has no more time, an alert will show.
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Turen er ovre.", message: "Kan ikke spise mere idag.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                    
                }
            }
            if atPoint(location) == childNode(withName: "buttonExam"){
                if let requiredKnowledge = requiredKnowledgeForNextClassLevel(){
                    if (playerCharacter.educationalKnowledge >= requiredKnowledge ){
                        playerCharacter.educationalKnowledge -= requiredKnowledge
                        examDeterminer()
                    }
                    else {
                        let examAttemptAlert: UIAlertController = UIAlertController(title: "Ikke nok viden", message: "Du mangler " + String(requiredKnowledge-playerCharacter.educationalKnowledge) + " viden for at kunne komme til eksamen.", preferredStyle: .alert)
                        examAttemptAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                            action in examAttemptAlert.dismiss(animated: true, completion: nil)}))
                        self.view?.window?.rootViewController?.present(examAttemptAlert, animated: true, completion: nil)
                    }
                } else {
                    print("Cannot retrieve required knowledge.")
                }
            }
            
        }
    }
    func requiredKnowledgeForNextClassLevel() -> Int?{
        switch playerCharacter.educationalLevel{
        case 1:
            return 10
        case 2:
            return 40
        case 3:
            return 90
        case 4:
            return 110
        case 5:
            return 160
        case 6:
            return 220
        case 7:
            return 300
        case 8:
            return 400
        case 9:
            return 500
        case 10:
            return 700
        case 11:
            return 800
        default:
            print("Cannot load required knowledge")
        }
        return nil
    }
    
    func examDeterminer() {
        if let characterQuestions = JsonLoader.sharedInstance.retrieveGameData()?.characterQuestions{
            switch playerCharacter.educationalLevel {
            case 1:
                startExam(firstCharacterQuestion: characterQuestions[0], secondCharacterQuestion: characterQuestions[1])
            case 2:
                startExam(firstCharacterQuestion: characterQuestions[2], secondCharacterQuestion: characterQuestions[3])
            case 3:
                startExam(firstCharacterQuestion: characterQuestions[4], secondCharacterQuestion: characterQuestions[5])
            case 4:
                startExam(firstCharacterQuestion: characterQuestions[6], secondCharacterQuestion: characterQuestions[7])
            case 5:
                startExam(firstCharacterQuestion: characterQuestions[8], secondCharacterQuestion: characterQuestions[9])
            case 6:
                startExam(firstCharacterQuestion: characterQuestions[10], secondCharacterQuestion: characterQuestions[11])
            case 7:
                startExam(firstCharacterQuestion: characterQuestions[12], secondCharacterQuestion: characterQuestions[13])
            case 8:
                startExam(firstCharacterQuestion: characterQuestions[14], secondCharacterQuestion: characterQuestions[15])
            case 9:
                startExam(firstCharacterQuestion: characterQuestions[16], secondCharacterQuestion: characterQuestions[17])
            default:
                print("No character question found for educational level.")
            }
        } else {
            print("Cannot find character questions.")
        }
        
    }
    
    func startExam(firstCharacterQuestion: CharacterQuestion, secondCharacterQuestion: CharacterQuestion){
        let randNumb = (Double(arc4random())/Double(UINT32_MAX))
        if (randNumb < 0.5){
            createAlertViewFor(characterQuestion: firstCharacterQuestion)
        } else {
            createAlertViewFor(characterQuestion: secondCharacterQuestion)
        }
    }
    // Creates view for the answer
    func createAlertViewFor(characterQuestion: CharacterQuestion){
        // Question alert view
        let examAlert: UIAlertController = UIAlertController(title: "Eksamen", message: characterQuestion.question, preferredStyle: .alert)
        examAlert.addAction(UIAlertAction(title: characterQuestion.answerFirst, style: UIAlertActionStyle.default, handler: {
            action in self.checkCorrectAnswer(chosenAnswer: characterQuestion.answerFirst, correctAnswer: characterQuestion.trueAnswer, examAlert: examAlert)}))
        examAlert.addAction(UIAlertAction(title: characterQuestion.answerSecond, style: UIAlertActionStyle.default, handler: {
            action in self.checkCorrectAnswer(chosenAnswer: characterQuestion.answerSecond, correctAnswer: characterQuestion.trueAnswer, examAlert: examAlert)}))
        examAlert.addAction(UIAlertAction(title: characterQuestion.answerThird, style: UIAlertActionStyle.default, handler: {
            action in self.checkCorrectAnswer(chosenAnswer: characterQuestion.answerThird, correctAnswer: characterQuestion.trueAnswer, examAlert: examAlert)}))
        self.view?.window?.rootViewController?.present(examAlert, animated: true, completion: nil)
    }
    
    // Checks for the correct answer
    func checkCorrectAnswer(chosenAnswer: String, correctAnswer: String, examAlert: UIAlertController){
        examAlert.dismiss(animated: true, completion: nil)
        
        // If answer is correct
        if(chosenAnswer == correctAnswer) {
            playerCharacter.educationalLevel += 1
            if(playerCharacter.educationalLevel >= 10){
                leaveScene()
            } else {
            let examAlert: UIAlertController = UIAlertController(title: "Resultat", message: "Tillykke du bestod eksamen og har nu opnået " + String(playerCharacter.educationalLevel) + " klasse!", preferredStyle: .alert)
            examAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                action in examAlert.dismiss(animated: true, completion: nil)}))
            self.view?.window?.rootViewController?.present(examAlert, animated: true, completion:nil)
            }
        } else {
            let examAlert: UIAlertController = UIAlertController(title: "Resultat", message: "Desværre bestod du ikke eksamen denne gang.", preferredStyle: .alert)
            examAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                action in examAlert.dismiss(animated: true, completion: nil)}))
            self.view?.window?.rootViewController?.present(examAlert, animated: true, completion: nil)
        }
        
        UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        updateBlackBoard()
    }
    
    func updateBlackBoard(){
        let classBoardNode = childNode(withName: "classBoardLabel") as! SKLabelNode
        classBoardNode.text = "Du går nu i " + String(playerCharacter.educationalLevel) + " klasse."
    }
    
    func leaveScene(){
        let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
        
        // Put the playercharacter in the context when returning.
        previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
        
        // Present the scene
        view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
    }
}


