//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit

class HomeController: SKScene{
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode?
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        let characterFigureData = self.userData?.value(forKey: "passedFigureData") as? CharacterSelectorFigure
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
        
        // Character in scene
        let characterFigureNode = childNode(withName: "characterSprite") as! SKSpriteNode
        characterFigureNode.texture = SKTexture(image: (characterFigureData?.figurePictureFull)!)
        characterFigureNode.size = CGSize(width: 300, height: 700)
        characterFigureNode.zPosition = 2
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
                
                // Put the playercharacter in the context when returning.
                previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
                
                // Present the scene
                view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
            }
        }
    }
}


