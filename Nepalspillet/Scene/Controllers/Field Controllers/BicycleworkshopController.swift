//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit

class BicycleworkshopController: SKScene{
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode!
    
    // Game specific values
    let BIKE_PRICE = 200
    let MONEY_PER_SESSION_WORK = 5
    let TIME_PER_SESSION_WORK = 1
    let REQUIRED_EDUCATIONAL_LEVEL_WORK = 3
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        let characterFigureData = self.userData?.value(forKey: "passedFigureData") as? CharacterSelectorFigure
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
        
        // Character in scene
        let characterFigureNode = childNode(withName: "characterSprite") as! SKSpriteNode
        characterFigureNode.texture = SKTexture(image: (characterFigureData?.figurePictureFull)!)
        characterFigureNode.size = CGSize(width: 300, height: 700)
        characterFigureNode.zPosition = 2
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
                
                // Put the playercharacter in the context when returning.
                previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
                
                // Present the scene
                view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
            }
            
            // Small cheat easter egg
            if atPoint(location) == childNode(withName: "BicycleNode"){
                playerCharacter.money = 10000
                playerCharacter.food = 10000
                playerCharacter.educationalKnowledge = 10000
                UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
            }
            
            // If buy button is pressed
            if atPoint(location) == childNode(withName: "buttonBuy"){
                if(playerCharacter.hasBike == true) {
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Har en cykel", message: playerCharacter.name + " har allerede en cykel.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                } else if(playerCharacter.money >= BIKE_PRICE) {
                    playerCharacter.money -= BIKE_PRICE
                    playerCharacter.hasBike = true
                    
                    // Update UI and show message
                    UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                    view?.showMessage("Du har købt en cykel til " + String(BIKE_PRICE) + "kr!", type: .info, options: [.position(.top)])
                } else {
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Har ikke råd", message: "Har ikke råd til en cykel, den koster " + String(BIKE_PRICE) + "kr.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                }
            }
            if atPoint(location) == childNode(withName: "buttonWork"){
                if(playerCharacter.educationalLevel >= REQUIRED_EDUCATIONAL_LEVEL_WORK){
                    if(playerCharacter.time >= TIME_PER_SESSION_WORK) {
                        playerCharacter.work(earnedMoney: MONEY_PER_SESSION_WORK, timeTaken: TIME_PER_SESSION_WORK)
                        
                        UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                        view?.showMessage("Har tjent: " + String(MONEY_PER_SESSION_WORK) + "kr.", type: .info, options: [
                            .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                            .cornerRadius(10),
                            .autoHideDelay(1)])
                    
                    } else {
                        // If the character has no more time, an alert will show.
                        let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Ikke nok tid!", message: "Der er ikke nok tid til at arbjede.", preferredStyle: .alert)
                        noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                            (alertAction: UIAlertAction!) in
                            noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                        self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                    }
                } else {
                    // If the character has no more time, an alert will show.
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "For lavt klassetrin!", message: "Skal minimum være i " + String(REQUIRED_EDUCATIONAL_LEVEL_WORK) + " klasse for at kunne arbejde her.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                }
            }
        }
    }
    
}


