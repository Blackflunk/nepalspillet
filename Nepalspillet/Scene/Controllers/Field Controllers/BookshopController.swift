//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit

class BookshopController: SKScene{
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode!
    
    // Game variables
    let REQUIRED_EDUCATIONAL_LEVEL_WORK = 6
    let MONEY_PER_SESSION_WORK = 12
    let TIME_PER_SESSION_WORK = 1
    let BOOKS_RECHARGE_TIME = 5
    let BOOK_KNOWLEDGE_GAIN = 10
    let BOOK_PRICE = 30
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
                
                // Put the playercharacter in the context when returning.
                previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
                
                // Present the scene
                view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
            }
            if atPoint(location) == childNode(withName: "buttonWork"){
                if(playerCharacter.educationalLevel >= REQUIRED_EDUCATIONAL_LEVEL_WORK){
                    if(playerCharacter.time >= TIME_PER_SESSION_WORK) {
                        playerCharacter.work(earnedMoney: MONEY_PER_SESSION_WORK, timeTaken: TIME_PER_SESSION_WORK)
                        
                        UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                        view?.showMessage("Har tjent: " + String(MONEY_PER_SESSION_WORK) + "kr", type: .info, options: [
                            .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                            .cornerRadius(10),
                            .autoHideDelay(1)])
                        
                    } else {
                        // If the character has no more time, an alert will show.
                        let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Ikke nok tid!", message: "Der er ikke nok tid til at arbjede.", preferredStyle: .alert)
                        noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                            (alertAction: UIAlertAction!) in
                            noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                        self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                    }
                } else {
                    // If the character has no more time, an alert will show.
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "For lavt klassetrin!", message: "Skal minimum være i " + String(REQUIRED_EDUCATIONAL_LEVEL_WORK) + " klasse for at kunne arbejde her.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                }
            }
            
            // A book will give you
            if atPoint(location) == childNode(withName: "buttonBuyBook"){
                if(playerCharacter.money >= BOOK_PRICE){
                    if(playerCharacter.booksBoughtInRoundNumber + BOOKS_RECHARGE_TIME <= playerCharacter.turn){
                        playerCharacter.money -= BOOK_PRICE
                        playerCharacter.educationalKnowledge += BOOK_KNOWLEDGE_GAIN
                        playerCharacter.booksBoughtInRoundNumber = playerCharacter.turn
                        
                        // Show message when book is purchased
                        UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                        view?.showMessage("Du har købt en bog for " + String(BOOK_PRICE) + "kr og fået " + String(BOOK_KNOWLEDGE_GAIN) + " viden.", type: .info, options: [.position(.top)])
                        
                    } else {
                        // If the character didn't wait for the book to be available
                        let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Bogen er ikke på lager!", message: "Du skal vente " + String((playerCharacter.booksBoughtInRoundNumber + BOOKS_RECHARGE_TIME) - playerCharacter.turn) + " ture for at kunne købe en ny bog.", preferredStyle: .alert)
                        noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                            (alertAction: UIAlertAction!) in
                            noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                        self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                    }
                } else {
                    // If the character can't afford book
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "For lidt penge!", message: "Du skal minimum have " + String(BOOK_PRICE) + "kr for at kunne købe en bog.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
                }
            }
        }
    }
}


