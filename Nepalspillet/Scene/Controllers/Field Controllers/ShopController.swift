//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit
import GSMessages

class ShopController: SKScene{
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode!
    var nextItemPrice: Int!
    var nextItemText: String!
    var buyItemButton: SKSpriteNode!
    var buyItemButtonText: SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        let characterFigureData = self.userData?.value(forKey: "passedFigureData") as? CharacterSelectorFigure
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
        
        buyItemButton = childNode(withName: "buyButton") as! SKSpriteNode
        buyItemButtonText = buyItemButton.childNode(withName: "buyLabel") as! SKLabelNode
        nextItemDeterminer()
        
        // Character scene
        let characterFigureNode = childNode(withName: "characterSprite") as! SKSpriteNode
        characterFigureNode.texture = SKTexture(image: (characterFigureData?.figurePictureFull)!)
        characterFigureNode.size = CGSize(width: 300, height: 700)
        characterFigureNode.zPosition = 2
        
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
        
                // Put the playercharacter in the context when returning.
                previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
                
                // Present the scene
                view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
            }
            // If buy item button is pressed
            if atPoint(location) == buyItemButton || atPoint(location) == buyItemButtonText{
                buyItem()
            }
        }
    }
    
    // Function to buy items
    func buyItem(){
        if(playerCharacter.money >= nextItemPrice){
            playerCharacter.money = playerCharacter.money - nextItemPrice
        if(playerCharacter.educationPencil == 0){
            playerCharacter.educationPencil = 20
             view?.showMessage("Du har købt nye blyanter for 10kr!", type: .info, options: [.position(.top)])
        } else if (playerCharacter.educationNotebook == 0){
            playerCharacter.educationNotebook = 40
             view?.showMessage("Du har købt et kladdehæfte for 20kr!", type: .info, options: [.position(.top)])
        } else if(playerCharacter.educationCalculator == 0){
            playerCharacter.educationCalculator = 120
             view?.showMessage("Du har købt en lommeregner for 200kr", type: .info, options: [.position(.top)])
            }
        }else{
            view?.showMessage("Du har ikke nok penge, " + nextItemText.lowercased() + " for " + String(nextItemPrice) + "kr.", type: .info, options: [.position(.top)])
        }
        UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        nextItemDeterminer()
    }
    
    // To determine the next price and what text the button should have
    func nextItemDeterminer(){
        if(playerCharacter.educationPencil == 0){
            buyItemButton.alpha = 1
            nextItemPrice = 10
            nextItemText = "Køb blyanter"
        } else if (playerCharacter.educationNotebook == 0){
            buyItemButton.alpha = 1
            nextItemPrice = 20
            nextItemText = "Køb kladdehæfte"
        } else if (playerCharacter.educationCalculator == 0){
            buyItemButton.alpha = 1
            nextItemPrice = 200
            nextItemText = "Køb lommeregner"
        } else {
            buyItemButton.alpha = 0
        }
        buyItemButtonText.text = nextItemText
    }
    
}


