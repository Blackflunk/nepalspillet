//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit
import GSMessages

class RiceController: SKScene{
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode!
    var MONEY_PER_SESSION = 3
    var TIME_PER_SESSION = 1
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        let characterFigureData = self.userData?.value(forKey: "passedFigureData") as? CharacterSelectorFigure
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
        
        // Character in scene
        let characterFigureNode = childNode(withName: "characterSprite") as! SKSpriteNode
        characterFigureNode.texture = SKTexture(image: (characterFigureData?.figurePictureFull)!)
        characterFigureNode.size = CGSize(width: 300, height: 700)
        characterFigureNode.zPosition = 2
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                // Fetch the old scene from the userData
                let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
                
                // Put the playercharacter in the context when returning.
                previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
                
                // Present the scene
                view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(1)))
            }
            
            if atPoint(location) == childNode(withName: "buttonWork"){
                // As long as the player has turns left, the player is able to work.
                if(playerCharacter.time > 0){
                    playerCharacter.work(earnedMoney: MONEY_PER_SESSION, timeTaken: TIME_PER_SESSION)
                    UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                    view?.showMessage("Har tjent: " + String(MONEY_PER_SESSION) + "kr", type: .info, options: [
                        .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                        .cornerRadius(10),
                        .autoHideDelay(1)])
                } else {
                    // If the character has no more time, an alert will show.
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Turen er ovre.", message: "Kan ikke arbejde mere idag.", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)

                }
                
            }
        }
    }
}


