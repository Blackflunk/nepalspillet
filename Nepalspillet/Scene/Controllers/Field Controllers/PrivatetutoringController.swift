//
//  HomeController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit

class PrivatetutoringController: SKScene{
    // Define game specific items from previous scene
    var playerCharacter: PlayerCharacter!
    var topBar: SKNode!
    let KNOWLEDGE_PER_SESSION = 1
    let TIME_PER_SESSION = 1
    
    override func didMove(to view: SKView) {
        // Instantiates the HUD
        topBar = UIHelper.sharedInstance.createTopBarUI(scene: self)
        playerCharacter = self.userData?.value(forKey: "characterInformation") as? PlayerCharacter
        let characterFigureData = self.userData?.value(forKey: "passedFigureData") as? CharacterSelectorFigure
        
        if let topBar = topBar {
            UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
        }
        
        // Character in scene
        let characterFigureNode = childNode(withName: "characterSprite") as! SKSpriteNode
        characterFigureNode.texture = SKTexture(image: (characterFigureData?.figurePictureFull)!)
        characterFigureNode.size = CGSize(width: 300, height: 700)
        characterFigureNode.zPosition = 2
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            
            // Check if back button is pressed, 4th place in the array.
            if atPoint(location) == topBar?.children[4]{
                let previousGameScene = self.userData?.value(forKey: "mainGameController") as? MainGameController
                
                // Put the playercharacter in the context when returning.
                previousGameScene?.userData = ["returnedPlayerCharacter": playerCharacter]
                
                // Present the scene
                view!.presentScene(previousGameScene!, transition: SKTransition.crossFade(withDuration: TimeInterval(0.5)))
            }
            if atPoint(location) == childNode(withName: "buttonTutoring"){
                if(playerCharacter.time > 0){
                // Can get tutoring while there's something to understand
                if(playerCharacter.forgottenKnowledge > 0){
                    playerCharacter.study(earnedKnowledge: KNOWLEDGE_PER_SESSION, timeTaken: TIME_PER_SESSION, ungainedKnowledge: 0)
                    playerCharacter.forgottenKnowledge -= 1
                    UIHelper.sharedInstance.updateHUD(labels: topBar.children, playerCharacter: playerCharacter)
                    view?.showMessage("Har fået: " + String(KNOWLEDGE_PER_SESSION) + " viden.", type: .info, options: [
                        .margin(.init(top: 60, left: 50, bottom: 0, right: 50)),
                        .cornerRadius(10),
                        .autoHideDelay(1)])
                } else {
                    let noMoreMissingKnowledgeAlert: UIAlertController = UIAlertController(title: "Ingen glemt viden", message: "Der er ikke mere at lære.", preferredStyle: .alert)
                    noMoreMissingKnowledgeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreMissingKnowledgeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreMissingKnowledgeAlert, animated: true, completion: nil)

                }
                
                } else {
                    let noMoreTimeAlert: UIAlertController = UIAlertController(title: "Ikke mere tid!", message: "Du har ikke mere tid denne uge!", preferredStyle: .alert)
                    noMoreTimeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {
                        (alertAction: UIAlertAction!) in
                        noMoreTimeAlert.dismiss(animated: true, completion: nil)}))
                    self.view?.window?.rootViewController?.present(noMoreTimeAlert, animated: true, completion: nil)
            }
            }
        }
    }
}


