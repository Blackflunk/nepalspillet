//
//  GameViewController.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 05/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    if let skView = self.view as! SKView? {
        if let scene = MainMenuController(fileNamed: "MainMenuScene"){
            // Scale the window to fit.
            scene.scaleMode = .aspectFill
            
            // Present the scene
            skView.presentScene(scene)
        }
        
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        }
    }


    override var prefersStatusBarHidden: Bool {
        return true
    }
}
