//
//  JsonHelper.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 26/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import UIKit

public class JsonLoader {
    // Uses a static shared instance, since it is a helper class and doesn't need to be initiated several times
    static let sharedInstance = JsonLoader()
    
    func retrieveGameData () -> CharactersInformation?{
        // Take asset from assets folder
        if let asset = NSDataAsset(name: "grunddata") {
            let data = asset.data
            // Use Swift 4 JSONDecoder
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            let decodedJson = try! decoder.decode(CharactersInformation.self, from: data)
            return decodedJson
        }
        return nil
    }
    
    // Extract game data from single character
    func extractGameDataFrom(characterName: String) -> Karakter?{
        if let gameData = retrieveGameData(){
            for karakter in gameData.figurer{
                if(karakter.navn == characterName){
                    return karakter
                }
            }
        }
        return nil
    }
    
}
