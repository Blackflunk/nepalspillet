//
//  UICreator.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 12/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

class UIHelper{
    static let sharedInstance = UIHelper()
    
    // The following funtion creates an item in the top bar to fit dimensions
    func topBarItemCreator(xPos: Int, label: SKLabelNode, itemIcon: SKSpriteNode) -> SKLabelNode {
        // Creates an item that holds a picture and a label, places it correctly in the top bar.
        label.text = "0"
        label.fontColor = SKColor.black
        label.position = CGPoint(x: xPos, y: -15)
        label.fontSize = 40
        label.alpha = 1
        label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.right
        
        itemIcon.size = CGSize(width: 40, height: 40)
        
        // Positioning the icon is a bit trial and error in order to place it correctly lined up with the label
        itemIcon.position = CGPoint(x: 25, y: 15)
        label.addChild(itemIcon)
        
        return label
    }
    
    func createTopBarUI(scene: SKScene) -> SKNode{
        // First add the bar itself
        let topBarColor = UIColor(white: 1, alpha: 0.5)
        let topBarNode = SKSpriteNode(color: topBarColor, size: CGSize(width: 1337, height: 70))
        topBarNode.position = CGPoint(x: 0, y: (scene.size.height/2)-(topBarNode.size.height/2))
        topBarNode.zPosition = 5
        
        // Then children of the top bar (Food, Educational level, Money, remaining turns)
        //Food label
        let foodItemNode = topBarItemCreator(xPos: -300, label: SKLabelNode(fontNamed: "Arial"), itemIcon: SKSpriteNode(imageNamed: "ikon_mad"))
        
        let educationalLevelNode = topBarItemCreator(xPos: -100, label: SKLabelNode(fontNamed: "Arial"), itemIcon: SKSpriteNode(imageNamed: "ikon_viden"))
        
        let moneyNode = topBarItemCreator(xPos: 100, label: SKLabelNode(fontNamed: "Arial"), itemIcon: SKSpriteNode(imageNamed: "ikon_penge"))
        
         let remainingTurnsNode = topBarItemCreator(xPos: 300, label: SKLabelNode(fontNamed: "Arial"), itemIcon: SKSpriteNode(imageNamed: "ikon_ur"))
        
        // Back button
        let backNode = SKSpriteNode(imageNamed: "ikon_tilbage")
        backNode.position = CGPoint(x: -600, y: 0)
        backNode.size = CGSize(width: 50, height: 50)
        backNode.zPosition = 6
        
        // Question button
        let questionNode = SKSpriteNode(imageNamed: "ikon_hjaelp")
        questionNode.position = CGPoint(x: 600, y: 0)
        questionNode.size = CGSize(width: 50, height: 50)
        questionNode.zPosition = 6
    
        // Add all the items to the top bar
        topBarNode.addChild(foodItemNode)
        topBarNode.addChild(educationalLevelNode)
        topBarNode.addChild(moneyNode)
        topBarNode.addChild(remainingTurnsNode)
        topBarNode.addChild(backNode)
        topBarNode.addChild(questionNode)
        
        
        
        scene.addChild(topBarNode)
        
        return topBarNode
    }
    
    func updateHUD(labels: [SKNode], playerCharacter: PlayerCharacter){
        // Elements as follows: Food, Knowledge, Money, Remaining turns
        (labels[0] as! SKLabelNode).text = String(describing: playerCharacter.food)
        (labels[1] as! SKLabelNode).text = String(describing: playerCharacter.educationalKnowledge)
        (labels[2] as! SKLabelNode).text = String(describing: playerCharacter.money)
        (labels[3] as! SKLabelNode).text = String(playerCharacter.time)
    }
    
    
}
