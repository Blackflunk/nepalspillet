//
//  QuickestPathFinder.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 29/10/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class QuickestPathFinder {
    // Uses a static shared instance, since it is a helper class and doesn't need to be initiated several times
    static let sharedInstance = QuickestPathFinder()
    
    // Following function finds the quickest path given a start position and end position, it returns the quickest path as an array of fields.
    func findPath(fields: [SKNode], startPos: SKNode, endPos: SKNode) -> [SKNode]?{
        
        let startPosIndex: Int? = fields.index(of: startPos)
        let endPosIndex: Int? = fields.index(of: endPos)
        
        var tempPath1: [SKNode] = []
        var tempPath2: [SKNode] = []
        
        if var pathCounter = startPosIndex{
            // Path 1
            while pathCounter != endPosIndex! {
                pathCounter = (pathCounter + 1)%fields.count
                tempPath1.append(fields[pathCounter])
            }
        }
 
        if var pathCounter = startPosIndex{
            //Path 2
            while pathCounter != endPosIndex! {
                pathCounter = ModulusHelper.sharedInstance.mod(pathCounter-1, fields.count)
                tempPath2.append(fields[pathCounter])
            }
        }
        
        // Figure out which path to use
        if(tempPath1.count <= tempPath2.count){
            return tempPath1
        } else {
            return tempPath2
        }
    }
    
    

}
