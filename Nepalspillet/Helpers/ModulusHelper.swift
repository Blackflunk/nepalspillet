//
//  ModulusHelper.swift
//  Nepalspillet
//
//  Created by Alexander V. Pedersen on 30/11/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import Foundation

public class ModulusHelper {
    // Uses a static shared instance, since it is a helper class and doesn't need to be initiated several times
    static let sharedInstance = ModulusHelper()
    
    // Following code is used from https://stackoverflow.com/questions/41180292/negative-number-modulo-in-swift answered by Martin R in order to find the positive modulus only.
    func mod(_ a: Int, _ n: Int) -> Int {
        precondition(n > 0, "modulus must be positive")
        let r = a % n
        return r >= 0 ? r : r + n
    }
}
