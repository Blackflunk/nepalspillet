//
//  NepalspilletTests.swift
//  NepalspilletTests
//
//  Created by Alexander V. Pedersen on 01/12/2017.
//  Copyright © 2017 Alexander V. Pedersen. All rights reserved.
//

import XCTest
@testable import Nepalspillet

class NepalspilletTests: XCTestCase {
    // Define variables for test
    var playerCharacter: PlayerCharacter!
    var characterInformation: Karakter!
    let MONEY_TO_INCREASE: Int = 3
    let TIME_TO_INCREASE: Int = 3
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        // Testing for Kamal information
        characterInformation = JsonLoader.sharedInstance.extractGameDataFrom(characterName: "Kamal")
        playerCharacter = PlayerCharacter(name: (characterInformation?.navn)!, money: (characterInformation?.startpenge)!, characterInformation: characterInformation)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testInitialMoneyWork(){
        // Check if initial money is correct
        XCTAssertEqual(characterInformation.startpenge, playerCharacter.money)
    }
    
    func testIncrementInMoneyWork(){
        // Try to work and see if the money increased
        playerCharacter.work(earnedMoney: MONEY_TO_INCREASE, timeTaken: TIME_TO_INCREASE)
        XCTAssertEqual(playerCharacter.money, characterInformation.startpenge + MONEY_TO_INCREASE)
        // 19 is the start ammount read from the PlayerCharacter class
        XCTAssertEqual(playerCharacter.time, 19 - TIME_TO_INCREASE)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
